/*$(function(){
	//判断页面是否是在微信浏览器打开
	//对浏览器的UserAgent进行正则匹配，不含有微信独有标识的则为其他浏览器
    var useragent = navigator.userAgent;
    if (useragent.match(/MicroMessenger/i) != 'MicroMessenger') {
        window.location.href = "app/wxError.jsp";//若不是微信浏览器，跳转到温馨error页面
    }
});*/

//金额分转元
function moneyF2Y(num){
    num = (num/100).toFixed(2);
    num = parseFloat(num)
    num = num.toLocaleString();
    return num;//返回的是字符串23,245.12保留2位小数
}

/**
 * Created by wenhao.
 */
postURL = function (URL, PARAMS) {
	var temp = document.createElement("form");
	temp.action = URL;
	temp.method = "post";
	temp.style.display = "none";
	for ( var x in PARAMS) {
		var opt = document.createElement("textarea");
		opt.name = x;
		opt.value = PARAMS[x];
		temp.appendChild(opt);
	}
	document.body.appendChild(temp);
	temp.submit();
};

function alertmBox(message){
	mBox.open({
		title: ['温馨提示','color:#161616;font-family:PingFang-SC-Bold;border-bottom: 1px solid #EBEBEB;text-align:center;padding-left:0'],
		content: message,
		btnName: ['确认'],
        btnStyle:["color:#46CC5E"],
		yesfun: function(){
			mBox.closeAll(); // 它将关闭所有层
		}
	});
}

function alertmBoxWithF(message,url){
	mBox.open({
		title: ['温馨提示','color:#161616;font-family:PingFang-SC-Bold;border-bottom: 1px solid #EBEBEB;text-align:center;padding-left:0'],
		content: message,
		btnName: ['确认'],
		btnStyle:["color:#46CC5E"],
		yesfun: function(){
			window.location.href=url; 
		}
	});
}
function alertmBoxWithClose(message){
	mBox.open({
		title: ['温馨提示','color:#161616;font-family:PingFang-SC-Bold;border-bottom: 1px solid #EBEBEB;text-align:center;padding-left:0'],
		content: message,
		btnName: ['确认'],
        btnStyle:["color:#46CC5E"],
		yesfun: function(){
			WeixinJSBridge.call('closeWindow');
		}
	});
}
function alertBoxAsk4Bind(){
	//询问框
	mBox.open({
	    title: ['温馨提示','color:#161616;font-family:PingFang-SC-Bold;border-bottom: 1px solid #EBEBEB;text-align:center;padding-left:0'],
	    content: '绑定成功，是否留在当前页面?',
	    btnName: ['留下绑卡', '离开'],
        btnStyle:["color:#46CC5E"],
	    yesfun: function(index){
	    	$("input").attr("value","");//清空input标签内容
	        mBox.close(index);
	    },
	    nofun: function(){
	    	WeixinJSBridge.call('closeWindow');
	    }
	});
}

function loading(message){
	mBox.open({
		boxtype: 3,
	    conStyle: 'text-align:center;',
	    maskColor:"rgba(0,0,0,0.8)",
	    maskClose:false,
	    time: 2,
	    content: '<div class="jemboxloadspin"><div class="jemboxloading"></div></div><p style="line-height:20px;">'+message+'</p>'
	});
}
